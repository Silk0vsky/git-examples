package com.consg.kristina.arraylist;

import java.util.Arrays;

public class ArrayApp {

    public static void main(String[] args) {
        int[] digits = { 12,85,42,0,-74, 42 };
        digits = appendDigit(digits, 5);
        System.out.println(Arrays.toString(digits));
    }

    private static int[] appendDigit(int[] digits, int i) {
        int[] newDigits = new int[digits.length+1];
        for(int j = 0; j<digits.length; j++){
            newDigits[j] = digits[j];
        }
        newDigits[newDigits.length-1] = i;
        return newDigits;
    }

}
