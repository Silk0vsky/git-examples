package com.consg.kristina.interfaces;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class App {

    public static void main(String[] args) {
        int x = 10;
        int y = 55;

        Armed[] army = {
                new Tank("T-34"),
                new JetFighter("Vasia", "F-22"),
                new Tank("T-35"),
                new Tank("T-38"),
                new JetFighter("Petya", "F-22"),
                new JetFighter("Mary", "F-22")
        };

        for (Armed armed : army) {
            armed.fire(x, y);
        }

    }

}
