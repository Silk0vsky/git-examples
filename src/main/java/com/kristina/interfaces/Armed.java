package com.consg.kristina.interfaces;

public interface Armed {

    void fire(int x, int y);

}
