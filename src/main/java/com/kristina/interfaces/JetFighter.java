package com.consg.kristina.interfaces;

public class JetFighter implements Armed {

    private String name;
    private String model;

    public JetFighter(String name, String model) {
        this.name = name;
        this.model = model;
    }

    @Override
    public void fire(int x, int y) {
        System.out.println("JetFighter " + model + " - " + name + " fired target at[" + x + "," + y + "]");
    }
}
