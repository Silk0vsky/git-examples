package com.consg.kristina.interfaces;

public class Tank implements Armed {

    private String name;

    public Tank(String name) {
        this.name = name;
    }

    public void fire(int x, int y) {
        System.out.println("Tank " + name + " fired target at[" + x + "," + y + "]");
    }

    public  void recharge() {
        System.out.println("recharging!");
    }

}
