package com.consg.kristina.movable;

import java.util.ArrayList;
import java.util.List;

public class MovableApp {
    public static void main(String[] args) {

        Location l = new Location();
        List<Movable> usersAndCars = new ArrayList();
        usersAndCars.add(new Car());
        usersAndCars.add(new Car());
        usersAndCars.add(new User());
        usersAndCars.add(new User());

        for(int i = 0; i<usersAndCars.size(); i++) {
            l.setX(i); l.setY(i+1);
            usersAndCars.get(i).move(l);
        }

    }
}
