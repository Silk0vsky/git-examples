package com.consg.kristina.movable;

public class Car implements Movable{
    private int speed;
    private Location location;
    private String model;

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }


    @Override
    public void move(Location location) {
        this.location = location;
        System.out.println("x = " + location.getX() + " y = " + location.getY());
    }

    @Override
    public Location getLocation() {
       return location;
    }


}
