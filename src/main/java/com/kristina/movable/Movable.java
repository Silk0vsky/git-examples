package com.consg.kristina.movable;

public interface Movable {
    void move(Location l);
    Location getLocation();
}
