package com.consg.kristina.movable;

public class User implements Movable{
    private String name;
    private String surname;
    private int age;
    private Location location;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    public void move(Location location) {
       this.location = location;
       System.out.println("x = " + location.getX() + " y = " + location.getY());
    }

    @Override
    public Location getLocation() {
        return location;
    }

}
