package com.consg.kristina.mergesort;

public class MergeSort {

    public static void merge(int[] a, int begin, int end) {
        int mid = (begin + end) / 2;
        int[] t = new int[end - begin + 1];
        int index_1 = begin;
        int index_2 = mid + 1;
        int index_t = 0;
        while ((index_1 <= mid) || (index_2 <= end)) {
            if (index_1 > mid) {
                t[index_t++] = a[index_2++];
                continue;
            }
            if (index_2 > end) {
                t[index_t++] = a[index_1++];
                continue;
            }
            if (a[index_2] > a[index_1]) {
                t[index_t++] = a[index_1++];
                continue;
            } else {
                t[index_t++] = a[index_2++];
                continue;
            }
        }
        for (int i = 0; i < end - begin + 1; i++) {
            a[i + begin] = t[i];
        }
    }

    public static void sort(int[] a, int start, int finish) {
        if (start == finish) {
            return;
        }
        if(a.length<2){
            return;
        }
        int mid = (start + finish) / 2;
        sort(a, start, mid);
        sort(a,mid + 1, finish);
        merge(a, start, finish);
    }

    public static void main(String[] args) {

    }

}
