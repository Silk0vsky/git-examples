package com.consg.ira;

import java.util.Arrays;

public class MergeSorter {
    public static void main(String[] args) {
        long[] array1 = { 8, 0, -3, 14568, 6, 9, 8, -4, 2, -99, 43 };
        long[] result = mergesort(array1);
        System.out.println(Arrays.toString(result));
    }

    public static long[] mergesort(long[] array1) {
        long[] buffer1 = Arrays.copyOf(array1, array1.length);
        long[] buffer2 = new long[array1.length];
        long[] result = mergesortInner(buffer1, buffer2, 0, array1.length);
        return result;
    }

    public static long[] mergesortInner(long[] buffer1, long[] buffer2,
                                       int startIndex, int endIndex) {
        if (startIndex >= endIndex - 1) {
            return buffer1;
        }

        int middle = startIndex + (endIndex - startIndex) / 2;
        long[] sorted1 = mergesortInner(buffer1, buffer2, startIndex, middle);
        long[] sorted2 = mergesortInner(buffer1, buffer2, middle, endIndex);

        int index1 = startIndex;
        int index2 = middle;
        int destIndex = startIndex;
        long[] result = sorted1 == buffer1 ? buffer2 : buffer1;
        while (index1 < middle && index2 < endIndex) {
            result[destIndex++] = sorted1[index1] < sorted2[index2]
                    ? sorted1[index1++] : sorted2[index2++];
        }
        while (index1 < middle) {
            result[destIndex++] = sorted1[index1++];
        }
        while (index2 < endIndex) {
            result[destIndex++] = sorted2[index2++];
        }
        return result;
    }
}
