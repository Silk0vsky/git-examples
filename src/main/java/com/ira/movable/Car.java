package com.consg.ira.movable;

public class Car implements Movable{
    private String carName;
    private Location location;
    private String model;

    public Car (String carName, Location location, String model){
        this.carName = carName;
        this.location = location;
        this.model = model;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @Override
    public void move(Location location) {
        this.location = location;
    }

    @Override
    public Location getLocation() {
        int x = location.getX();
        int y = location.getY();
        System.out.println("x = " + x + " y = " + y);
        return location;
    }
}
