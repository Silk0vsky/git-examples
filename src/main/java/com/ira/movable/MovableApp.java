package com.consg.ira.movable;

import java.util.ArrayList;
import java.util.List;

public class MovableApp {
    public static void main(String[] args) {

        Location forest = new Location(6,7);
        Location home = new Location(13,13);

        Car car13 = new Car("Galant", forest, "R5");
        car13.getLocation();

        List<Movable> mishmash = new ArrayList<>();
        mishmash.add(new Car("Mitsubishi", home,"A3"));
        mishmash.add(new Car("Reno", forest,"Y9"));
        mishmash.add(new Car("Citroen", forest,"A5"));
        mishmash.add(new Car("Audi", forest,"I1"));
        mishmash.add(new User("Boo", "Bubu", 26, home));
        mishmash.add(new User("Boo1", "", 26, home));

        mishmash.get(4).getLocation();

    }
}
