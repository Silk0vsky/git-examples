package com.consg.ira.movable;

public interface Movable {
    void move(Location location);
    Location getLocation();
}
