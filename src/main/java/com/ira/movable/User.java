package com.consg.ira.movable;

public class User implements Movable{

    private String userName;
    private String userSurname;
    private int userAge;
    private Location location;

    public User (String userName, String userSurname, int userAge, Location location) {
        this.userName=userName;
        this.userSurname=userSurname;
        this.userAge=userAge;
        this.location=location;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserAge() {
        return userSurname;
    }

    public void setUserAge(int userAge) {
        this.userAge = userAge;
    }

    public void setUserSurname(String userSurname) {
        this.userSurname = userSurname;
    }

    public void getUserSurname(String userSurname) {
        this.userSurname = userSurname;
    }

    @Override
    public void move(Location location) {
        this.location = location;
    }

    @Override
    public Location getLocation() {
        int x = location.getX();
        int y = location.getY();
        System.out.println("x = " + x + " y = " + y);
        return location;
    }
}
