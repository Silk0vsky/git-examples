package com.consg.objects;

import java.util.Arrays;

class Person {
    private String name;
    private int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public int getAge() {
        return age;
    }
    public String getName() {
        return name;
    }

    public void introduse() {
        System.out.println(this.name);
    }

    public static boolean coupleCanMarry(Person person1, Person person2) {
        return person1.getAge() > 18 && person2.getAge() > 18;
    }
}


public class ObjectsApp {

    public static void main(String[] args) {
        int [] arr = new int[]{7,5,9,-5,4};
        Arrays.sort(arr);

        Person person1 = new Person("Vasya", 8);
        Person person2 = new Person("Mary", 34);

        System.out.println(Person.coupleCanMarry(person1, person2));
    }

}
