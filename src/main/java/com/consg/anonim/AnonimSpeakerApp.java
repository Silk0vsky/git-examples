package com.consg.anonim;

interface Speaker {
    void speak(String msg);
}

public class AnonimSpeakerApp {

    public static void introduceSpeaker(Speaker speaker) {
        speaker.speak("hello");
    }

    public static void main(String[] args) {

        Speaker speaker = new Speaker() {
            @Override
            public void speak(String msg) {
                System.out.println("speak: " + msg);
            }
        };
        speaker.speak("123");

    }

}
