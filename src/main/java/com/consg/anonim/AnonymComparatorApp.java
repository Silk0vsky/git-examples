package com.consg.anonim;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

class Person {
    private String name;

    public Person(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}


public class AnonymComparatorApp {

    public static void main(String[] args) {

        List<Person> people = new ArrayList<>();
        people.add(new Person("Alla"));
        people.add(new Person("Jack"));
        people.add(new Person("Molly"));
        people.add(new Person("Li"));
        people.add(new Person("Pennilopa"));

        Function<String, String> func = (arg) -> "prefix_" + arg;

        Collections.sort(people, new Comparator<Person>() {
            @Override
            public int compare(Person p1, Person p2) {
                return p1.getName().compareTo(p2.getName());
            }
        });

        Collections.sort(people, (p1, p2) -> p1.getName().compareTo(p2.getName()));

        for (Person person : people) {
            System.out.println(person.getName());
        }

    }

}
