package com.consg.threads;

import java.util.concurrent.Executors;

public class ThreadPoolApp {

    public static void main(String[] args) {
        Executors.newCachedThreadPool().submit(() -> {
            System.out.println("pool thread");
        });
    }

}
