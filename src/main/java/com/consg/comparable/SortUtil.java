package com.consg.comparable;

import java.util.Arrays;

public class SortUtil {

    public static <T extends Comparable> T[] bubleSort(T[] array) {
        for (int i=0; i< array.length; i++) {
            for (int j=i; j< array.length-1; j++) {
                if (array[j].compareTo(array[j+1]) > 0) {
                    T tmp=array[j];
                    array[j] = array[j+1];
                    array[j+1]=tmp;
                }
            }
        }
        return array;
    }

}
