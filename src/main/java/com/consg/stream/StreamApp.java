package com.consg.stream;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class StreamApp {

    public static void main(String[] args) {

        List<String> cities = Arrays.asList("Oslo", "Odessa", "Uzhgorod", "Helsinki", "Kiev", "Yokogama", "Lviv", "Striy");
        Map<String, Integer> collect = cities.stream()
                .filter(arg -> arg.length() > 5)
                .map(arg -> arg + "!")
                .collect(Collectors.toMap(
                        elem -> elem,
                        elem -> elem.length())
                );
        System.out.println(collect);

    }

}
