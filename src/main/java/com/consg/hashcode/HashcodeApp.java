package com.consg.hashcode;


import java.util.*;

class Article {
    private String text;

    public Article(String text) {
        this.text = text;
    }

    @Override
    public boolean equals(Object obj){
        if (obj == this) {
            return true;
        } else {
            if (obj instanceof Article) {
                Article art = (Article) obj;
                return art.text.equals(this.text);
            } else {
                return false;
            }
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(text);
    }

}

public class HashcodeApp {

    public static void main(String[] args) {
        Map<String, Article> strings = new HashMap<>();
        strings.put(new String("Odessa"), new Article("a"));
        strings.put(new String("Kiev"), new Article("b"));
        strings.put(new String("Lviv"), new Article("c"));
        strings.put(new String("Lviv"), new Article("c"));
        strings.forEach((key, val) -> System.out.println(key + " - " + val));

        System.out.println("-----------");

        HashSet<String> hashSet = new HashSet<>();
        hashSet.add("123");
        hashSet.add("123");

        Map<Article, String> tmp = new HashMap<>();
        tmp.put(new Article("a"), "123");
        tmp.put(new Article("b"), "23");
        tmp.put(new Article("c"), "789");
        tmp.put(new Article("d"), "555");
        tmp.put(new Article("d"), "555");
        tmp.forEach((key, val) -> System.out.println(key + " - " + val));

    }

}
