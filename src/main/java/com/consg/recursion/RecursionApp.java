package com.consg.recursion;

public class RecursionApp {

    public static int factorial(int x){
        int res=1;
        while (x>0) {
            res*=x; x--;
        }
        return res;
    }

    public static int factorial2(int x){
        if (x == 1) {
            return 1;
        } else {
            return factorial2(x-1) * x;
        }
    }

    public static void main(String[] args) {
        System.out.println(factorial2(10));
    }

}
