package com.consg.patterns.singleton;

import java.util.ArrayList;
import java.util.List;

class LazyPeopleRegistrar {

    private static LazyPeopleRegistrar instance;
    public static LazyPeopleRegistrar getInstance() {
        if (instance == null) {
            instance = new LazyPeopleRegistrar();
        }
        return instance;
    }

    private List<String> names = new ArrayList<>();
    private LazyPeopleRegistrar(){
        System.out.println("initializing");
    };


    public void register(String name){
        System.out.println("registering " + name);
        this.names.add(name);
    };
    public boolean isRegistered(String name) {
        return names.contains(name);
    }

}

public class LazySingletonApp {

    public static void main(String[] args) {
        LazyPeopleRegistrar registrar = LazyPeopleRegistrar.getInstance();
        registrar.register("Vasya");
        System.out.println(registrar.isRegistered("Vasya"));
        System.out.println(registrar.isRegistered("Mary"));

    }

}
