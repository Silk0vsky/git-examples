package com.consg.patterns.singleton;


import java.util.ArrayList;
import java.util.List;

class PeopleRegistrar {

    public static final String regName = "ZAGS";
    public static final PeopleRegistrar instance = new PeopleRegistrar();

    private List<String> names = new ArrayList<>();

    private PeopleRegistrar(){
        System.out.println("initializing");
    };

    public void register(String name){
        System.out.println("registering " + name);
        this.names.add(name);
    };

    public boolean isRegistered(String name) {
        return names.contains(name);
    }

}


public class InstantSingletonApp {

    public static void main(String[] args) {
        System.out.println(PeopleRegistrar.regName + " application!");
        System.out.println("line 33");
        PeopleRegistrar.instance.register("Vasya");
        System.out.println("line 36");
        System.out.println(PeopleRegistrar.instance.isRegistered("Vasya"));
        System.out.println(PeopleRegistrar.instance.isRegistered("Mary"));

    }

}
