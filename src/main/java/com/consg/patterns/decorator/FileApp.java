package com.consg.patterns.decorator;


import java.io.*;

public class FileApp {

    public static void main(String[] args) throws IOException {
        File file = new File("database.txt");

        long start = System.currentTimeMillis();
        Writer fw = new BufferedWriter(new FileWriter(file));
        for (int i = 0; i < 50_000_000; i++) {
            fw.write("hello\n");
        }
        fw.close();
        System.out.println(System.currentTimeMillis() - start);
    }

}
