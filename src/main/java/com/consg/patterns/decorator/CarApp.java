package com.consg.patterns.decorator;

interface Movable {
    public void move(String destination);
}

class Car implements Movable {
    @Override
    public void move(String destination) {
        System.out.println("moving to " + destination);
    }
}

class PoliceCar implements Movable {
    private Movable car;

    public PoliceCar(Movable car) {
        this.car = car;
    }

    @Override
    public void move(String destination) {
        System.out.println("уиууу-уиуу-уиуу ");
        car.move(destination);
    }
}

public class CarApp {

    public static void main(String[] args) {
        Movable movable = new PoliceCar(new Car());
        movable.move("Parish");
    }

}
