package com.consg.patterns.decorator;


import java.io.*;

public class StreamApp {

    public static void main(String[] args) throws IOException {
        File file = new File("database.txt");

        OutputStream os = new BufferedOutputStream(new FileOutputStream(file));
        for (int i = 0; i < 5_000; i++) {
            os.write("123\n".getBytes());
        }
        os.close();
    }

}
