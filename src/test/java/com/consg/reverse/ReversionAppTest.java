package com.consg.reverse;

import org.junit.Test;

import static org.junit.Assert.*;

public class ReversionAppTest {

    @Test
    public void reverse() {
        ReversionApp reversionApp = new ReversionApp();
        String exTest = "Hello";
        String example = reversionApp.reverse("olleH");
        assertEquals(exTest,example);
    }
}