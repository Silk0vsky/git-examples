package com.consg.kristina;

import com.consg.kristina.mergesort.MergeSort;
import org.junit.Test;

import static org.junit.Assert.*;

public class MergeSortTest {

    @Test
    public void sort() {
        MergeSort mer = new MergeSort();
        int[] test = new int[]{6,1,8,3,4};
        int[] array = new int[]{1,3,4,6,8};

        mer.sort(test,0,test.length-1 );

        assertArrayEquals(array,test);
    }
}