package com.consg.comparable;

import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertArrayEquals;

public class SortUtilTest {

    @Test
    public void sort() {
        Integer[] unsortedArray = new Integer[]{6,1,8,3,4};
        Integer[] sortedArray = new Integer[]{1,3,4,6,8};

        Integer[] ints = SortUtil.bubleSort(unsortedArray);
        assertArrayEquals(sortedArray, ints);
    }

    static class Person  {
        private String name;
        private Integer age;

        public Person(String name, Integer age) {
            this.name = name;
            this.age = age;
        }

        public String getName() {
            return name;
        }
        public void setName(String name) {
            this.name = name;
        }
        public Integer getAge() {
            return age;
        }
        public void setAge(Integer age) {
            this.age = age;
        }

    }

    @Test
    public void sortPersons() {
        Person[] people = {
                new Person("Mary", 17),
                new Person("John", 23),
                new Person("Katy", 13),
        };

        Arrays.sort(people, new Comparator<Person>() {
            @Override
            public int compare(Person person, Person t1) {
                return person.getAge().compareTo(t1.getAge());
            }
        });
        for (Person pers : people) {
            System.out.println(pers.getName() + " - " + pers.getAge());
        }
    }

}
